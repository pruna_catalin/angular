import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SocketService} from './_services/socket.services';
const AVATAR_URL = 'https://api.adorable.io/avatars/285';
import { Message } from './_models/message';
import {Timestamp} from 'rxjs/internal-compatibility';
import {AuthenticationService} from './_services';
import { CheckIntegrityService } from './_services/check-integrity.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  modules: string[];
  count = 0;
  constructor(private authenticationService: AuthenticationService, public router: Router, public socket: SocketService,
           private checkIntegrity: CheckIntegrityService) {
  }

  ngOnInit() {
    // this.socket.initSocket() ;
    // this.socket.onPong().subscribe((message: Message) => {
    //   this.checkIntegrity.checkIntegrity();
    // });
    // this.socket.onMessage().subscribe((message: Message) => {
    //   let cloneMsg = {success: false, message: ''};
    //   if (message.content instanceof ArrayBuffer) {
    //       const decodedString = String.fromCharCode.apply(null, new Uint8Array(message.content));
    //       cloneMsg = JSON.parse(decodedString);
    //   }
    //   if ( cloneMsg.success ) {
    //       console.log('Allowed', cloneMsg);
    //   }  else {
    //       console.log('logout from loginOnSocket', cloneMsg);
    //        this.authenticationService.logout();
    //   }
    // });
  }

  isLogin() {
    return true;
  }
  // logout() {
  //   this.authenticationService.logout();
  //   this.router.navigate(['/login']);
  //   return true;
  // }
  // private initModel(): void {
  //   const randomId = Guid.create().toString();
  //   this.user = {
  //     id: randomId,
  //     avatar: `${AVATAR_URL}/${randomId}.png`,
  //     username: randomId
  //   };
  //   if (localStorage.getItem('userId')) {
  //     this.user = {
  //       id: randomId,
  //       avatar: `${AVATAR_URL}/${randomId}.png`,
  //       username : randomId
  //     };
  //   } else {
  //     localStorage.setItem('userId', this.user.id);
  //     localStorage.setItem('username', randomId);
  //   }
  // }
  // private initIoConnection(): void {
  //   this.socketService.initSocket();
  //

  //   // Connect to Server
  //   this.socketService.onEvent(Event.CONNECT) // connect
  //       .subscribe(() => {
  //         this.sendMessage(' is here', 'messageOnConnect');
  //       });
  //   // Force logout on lost connection
  //   this.socketService.onEvent(Event.DISCONNECT)
  //       .subscribe(() => {
  //         this.messages.push({
  //           from: this.user,
  //           content: 'SERVER IS DEAD'
  //         });
  //         console.log('disconnected');
  //       });
  // }
  // public receiveMesssage (message: Message) {
  //   message.type = 'receive';
  //   console.log(message);
  //   this.messages.push(message);
  // }
  // public sendMessage(message: string, event): void {
  //   if (!message) {
  //     return;
  //   }
  //   const username = localStorage.getItem('userId');
  //   message = username + '( AKA Master):   ' + message;
  //   this.socketService.send({
  //     from: this.user,
  //     content: message
  //   }, event);
  //   this.messageContent = null;
  // }

}

