import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { MaintenanceComponent } from './maintenance/maintenance.component';
import {AuthGuard} from './_helpers';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'maintenance',
    component: MaintenanceComponent
  },
  {
    path: 'login',
    component: AuthComponent
  },
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./core/core.module').then(m => m.CoreModule),
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes , {
      enableTracing: false
    })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
