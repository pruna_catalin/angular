import { Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../_services';
import {DogWatcher} from '../../_services/watch.services';
import {CheckIntegrityService} from '../../_services/check-integrity.service';
declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export let ROUTES: RouteInfo[] =  [];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
    console = console;
  constructor(public translate: TranslateService, private router: Router,
                private authenticationService: AuthenticationService ,
                private  dog: DogWatcher, private integrity: CheckIntegrityService) {
      translate.setDefaultLang('en');
      translate.use('it');
  }
  ngOnInit() {
      const routes = localStorage.getItem('currentPermisions');
      if (routes !== null ) {
          ROUTES =  JSON.parse(routes).sidebar;
      } else {
          this.authenticationService.logout();
          this.router.navigate(['/login']);
      }
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
