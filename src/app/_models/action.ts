export enum Action {
    JOINED,
    LEFT,
    RENAME,
    Disconnect = 'ForceDisconnect'
}
