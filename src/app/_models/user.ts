import {Guid} from 'guid-typescript';

export interface User {
    id?: string;
    name?: string;
    avatar?: string;
    username?: string;
}
