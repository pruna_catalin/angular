
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { AppComponent } from './app.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';

import {ErrorInterceptor,  JwtInterceptor, AuthGuard} from './_helpers';

import {  AgmCoreModule } from '@agm/core';
import { CoreComponent } from './core/core.component';
import {  SharedModule  } from './shared-module/shared-module.module';
import { DogWatcher } from './_services/watch.services';
import { AlertService, AuthenticationService } from './_services';
import { SocketService } from './_services/socket.services';
import { CheckIntegrityService } from './_services/check-integrity.service';
import { MaintenanceComponent } from './maintenance/maintenance.component';
@NgModule({
  imports: [

      SharedModule,
      RouterModule,
      AppRoutingModule,
      AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    }),
      ComponentsModule
  ],
  declarations: [
    CoreComponent,
    AppComponent,
    MaintenanceComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
     DogWatcher , AlertService , SocketService , AuthenticationService , AuthGuard , CheckIntegrityService
    ],
  exports: [
      SharedModule
  ],
  entryComponents: [ CoreComponent ],
  bootstrap: [AppComponent]
})
export class AppModule { }
