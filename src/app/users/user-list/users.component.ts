import {Component, NgModule, OnInit, ViewChild} from '@angular/core';
import {DataTablesModule} from 'angular-datatables';
import {BrowserModule} from '@angular/platform-browser';
import { ModelUser } from '../model/ModelUser';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {DogWatcher} from '../../_services/watch.services';
import { SharedModule } from 'app/shared-module/shared-module.module';
declare var $;
console.log('before mata');
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

@NgModule({
    declarations: [],
    imports: [
        BrowserModule,
        SharedModule,
        DataTablesModule.forRoot()
    ],
    providers: [],
})
export class UsersComponent implements OnInit {
  @ViewChild('UserList', { read: true, static: false }) table;
  dataTable: any;
  persons: ModelUser[];
  dtOptions: DataTables.Settings = { autoWidth: true };
  constructor( private http: HttpClient, private watcher: DogWatcher) { }
  ngOnInit() {
    this.dataTable = $(this.table.nativeElement);
      this.dtOptions = {
          pagingType: 'full_numbers',
          serverSide: true,
          processing: true,
          ajax: (dataTablesParameters: any, callback) => {
                   this.watcher.CallApi({
                      method: 'POST',
                      target: '/sandbox',
                      headers: {
                          'Content-Type':  'application/json', 'X-Requested-With': 'XMLHttpRequest'
                      },
                      body: dataTablesParameters
                    }
                  ).then(function(data) {
                      console.log('callback : ', data);
                      callback(data);
                  }).catch(error => {
                      console.log('Error : ', error);
                      callback({recordsTotal: 0, recordsFiltered: 0, data: [] })
                  });
          },
          columns: [
              { data: 'id' },
              { data: 'name' }
          ],
      };
      this.dataTable.dataTable(this.dtOptions);
  }
}
export class UserDetailsComponent {}
export class SandboxComponent implements OnInit {
    paramsSub: any;
    id: any;
    constructor (private activatedRoute: ActivatedRoute) {}
    ngOnInit() {
        console.log('test my ass');
        this.paramsSub = this.activatedRoute.params.subscribe(params => this.id = parseInt(params['id'], 10));
    }
}


