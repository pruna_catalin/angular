import {Injectable, OnInit} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {DogWatcher} from './watch.services';
import {SocketService} from './socket.services';
import {Router} from '@angular/router';

@Injectable()
export class CheckIntegrityService {

  constructor(private authenticationService: AuthenticationService, private dog: DogWatcher) { }
  public checkIntegrity(ev: String) {
    const token = localStorage.getItem('currentToken');
    const username = localStorage.getItem('currentUsername');
    this.dog.CallApi({
          method: 'POST',
          target: '/checkIntegrity',
          headers: {
            'Content-Type':  'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest'
          },
          body: 'route=' + ev + '&token=' + token + '&username=' + username
        }
    ).then (data => {
      if (!data.success) {
        this.authenticationService.logout();
      }
    }).catch(error => {
      this.authenticationService.logout();
      console.log('Error  : ', error);
    });
    // console.log(ev.target.attributes.href,'check me for each click');
  }
}
