import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import {DogWatcher} from './watch.services';
import {AlertService} from './alert.service';
import { Router} from '@angular/router';
import {SocketService} from './socket.services';
import {Message} from '../_models/message';

import {CheckIntegrityService} from '../_services/check-integrity.service';
@Injectable()
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;
    private checkIntegrity: CheckIntegrityService;
    constructor(private http: HttpClient, private watcher: DogWatcher, private alertService: AlertService,
                private router: Router, public socket: SocketService) {
        console.log('GET FROM AUTH SERV', localStorage.getItem('currentUser') , localStorage.getItem('currentPermisions'));
    }

    public get currentUserValue() {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
        return this.currentUserSubject.value;
    }

    login(username, password, token, returnUrl) {
       return  this.watcher.CallApi({
            method: 'POST',
            target: '/users/authenticate',
            headers: {
                'Content-Type':  'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest'
            },
            body: 'Model[username]=' +  username +  '&Model[password]=' + password + '&Model[token]=' +  token
        }).then((data) => {
            if (data.success) {

                console.log('Data receive : ', data);
                localStorage.setItem('currentUser',  JSON.stringify(data.user));
                localStorage.setItem('currentUsername', data.user.username);
                localStorage.setItem('currentPermisions', JSON.stringify(data.permisions));
                localStorage.setItem('currentToken', data.user.token);
                this.currentUserSubject.next(data);
                this.router.navigate([returnUrl]);

                return {type: 'success', text: data.message};
            } else {
                if (data.serverFaild) {
                    return {type: 'error', text: 'Username and password wrong'};
                }
                return {type: 'error', text:    data.message};
            }
        }).catch( error => {
           return {type: 'error', text: error.message};
        });
    }
    checkLogin(segment: object) {
        const userData = localStorage.getItem('currentUsername');
        const userToken = localStorage.getItem('currentToken');
        if (userData !== null && userToken !== null) {
            console.log('check login', segment[0]);
            this.socket.send({ content: { 'username': userData , 'token': userToken }});
            return true;
        } else {
            console.log('Check Login from services');
           this.logout();
        }
    }
    logout() {
        this.socket.send({content : 'disconnect'}, 'disconnect');
        console.log('Logout from services');
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');
        localStorage.removeItem('currentUsername');
        localStorage.removeItem('currentPermisions');
        localStorage.removeItem('currentToken');
        this.currentUserSubject.next(null);
        this.router.navigate(['/login']);
    }
}
