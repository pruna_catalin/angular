import {Injectable, Component, ViewChild, ViewContainerRef} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Message } from '../_models/message';
import { Event } from '../_models/event';

import * as socketIo from 'socket.io-client';
import {Config} from '../_config/config';
import {Router, ActivatedRoute} from '@angular/router';

const SERVER_URL = Config.Service.broadcastLocation;

@Injectable()
export  class SocketService {
    private socket = null;
    public returnUrl = '/';
    constructor ( private router: Router ,  private route: ActivatedRoute ) {}
    public initSocket( ): void {
        const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
        const routerA = this.router;
        if ( this.socket == null ) {
            this.socket = socketIo(SERVER_URL, { timeout : 1000});
            const _socket = this.socket;
            this.socket.on('connect_error', function() {
                console.log('DA COIE AM PICAT');
                routerA.navigate(['maintenance'], { replaceUrl : false });
            });
            this.socket.on('connect' , function() {
               console.log('am revenit belimiai', returnUrl);
               routerA.navigate([returnUrl]);
            });
        }
    }
    public send(message: Message, event: String = 'message') {
        if (this.socket !== null) {
            this.socket.emit(event, message);
        } else {
            return {type: 'error', text: 'Socket faild'};
        }
    }
    public onMessage(eventName: String = 'message'): Observable<Message> {
        if (this.socket !== null) {
            return new Observable<Message>(observer => {
                this.socket.on(eventName, (data: Message) => observer.next(data));
            });
        } else {
            return new Observable<Message>(observer => {  });
        }
    }
    public onPong(eventName: String = 'pong'): Observable<Message> {
        console.log('PONG ');
        if (this.socket !== null) {
            return new Observable<Message>(observer => {
                this.socket.on(eventName, (data: Message) => observer.next(data));
            });
        } else {
            return new Observable<Message>(observer => {  });
        }
    }
    public onEvent(event: Event): Observable<any> {
        if (this.socket !== null) {
            return new Observable<Event>(observer => {
                this.socket.on(event, () => observer.next());
            });
        } else {
            return new Observable<Event>(observer => { });
        }
    }
}
