import { Config } from '../_config/config';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs-compat/add/operator/map';
import {Injectable} from '@angular/core';
import {Router, Routes} from '@angular/router';

@Injectable()
export class DogWatcher {
    constructor (private http: HttpClient, private router: Router) {    }
    /*
        Dynamic Methods
        Dynamic Params
        Dynamic Headers
        return Promise response
     */
    public  CallApi(params: any): any {
        params.url = Config.Service.apiLocation + params.target;
        params.options = {
            headers: new HttpHeaders( params.headers )
        };
        return new Promise((resolve, reject) => {
            return this.http.request( params.method, params.url, params ).toPromise()
                .then(
                    getResponse => {
                        const resultPromise = Config.IsDev ? console.log(
                            '======================Result from Request======================: ',
                            {'Type Request': params.method, 'Params' : params, 'Location': location}) : '';
                        resolve(getResponse);
                        return;
                    })
                .catch(error => {
                    console.log(error);
                    const resultPromise = Config.IsDev ? console.error(
                            '======================Error Result from Request ( SERVER IS DOWN )======================: ',
                            {'Type Request': params.method, 'Params' : params, 'Location': location, 'Error': 'SERVER IS DOWN'}) : '';
                    reject({ success: false, message: 'Server Down', action: 'goToLogin' });
                    this.router.navigate(['/login'], { queryParams: { returnUrl: '' }});
                    return;
                });
        });
    }

}
