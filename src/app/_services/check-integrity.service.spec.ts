import { TestBed } from '@angular/core/testing';

import { CheckIntegrityService } from './check-integrity.service';

describe('CheckIntegrityService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CheckIntegrityService = TestBed.get(CheckIntegrityService);
    expect(service).toBeTruthy();
  });
});
