import {AfterViewInit, Component, OnInit} from '@angular/core';


import {Action} from '../_models/action';
import {Event} from '../_models/event';
import {Message} from '../_models/message';
import {User} from '../_models/user';
import {SocketService} from '../_services/socket.services';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Guid} from 'guid-typescript';


const AVATAR_URL = 'https://api.adorable.io/avatars/285';

@Component({
  selector: 'app-tcc-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewInit {
  action = Action;
  user: User;
  chatForm: FormGroup;
  messages: Message[] = [];
  messageContent: string;
  ioConnection: any;
  // dialogRef: MatDialogRef<DialogUserComponent> | null;
  defaultDialogUserParams: any = {
    disableClose: true,
    data: {
      title: 'Welcome',
      // dialogType: DialogUserType.NEW
    }
  };

  constructor(private formBuilder: FormBuilder, private socketService: SocketService) {
  }

  ngOnInit(): void {
    this.initModel();
    this.initIoConnection();
    this.chatForm = this.formBuilder.group({  message: []  });

    // Using timeout due to https://github.com/angular/angular/issues/14748

  }

  ngAfterViewInit(): void { }
  get f() { return this.chatForm.controls; }
  onSubmit() {
    this.sendMessage(this.f.message.value, 'message');
    this.f.message.setValue('');
    return true;
  }
  private initModel(): void {
    const randomId = Guid.create().toString();
    this.user = {
      id: randomId,
      avatar: `${AVATAR_URL}/${randomId}.png`,
      username: randomId
    };
    if (localStorage.getItem('userId')) {
      this.user = {
        id: randomId,
        avatar: `${AVATAR_URL}/${randomId}.png`,
        username : randomId
      };
    } else {
      localStorage.setItem('userId', this.user.id);
      localStorage.setItem('username', randomId);
    }
  }
  private initIoConnection(): void {
    this.socketService.initSocket();

    this.ioConnection = this.socketService.onMessage()
        .subscribe((message: Message) => {
          this.receiveMesssage(message);
        });
    // Connect to Server
    this.socketService.onEvent(Event.CONNECT) // connect
        .subscribe(() => {
           this.sendMessage(' is here', 'messageOnConnect');
        });
    // Force logout on lost connection
    this.socketService.onEvent(Event.DISCONNECT)
        .subscribe(() => {
          this.messages.push({
            from: this.user,
            content: 'SERVER IS DEAD'
          });
          console.log('disconnected');
        });
  }
  private receiveMesssage (message: Message) {
    message.type = 'receive';
    console.log(message);
    this.messages.push(message);
  }
  public sendMessage(message: string, event): void {
    if (!message) {
      return;
    }
    const username = localStorage.getItem('userId');
    message = username + '( AKA Master):   ' + message;
    this.socketService.send({
      from: this.user,
      content: message
    }, event);
    this.messageContent = null;
  }

  public sendNotification(params: any, action: Action): void {
    let message: Message;

    if (action === Action.JOINED) {
      message = {
        from: this.user,
        action: action
      }
    } else if (action === Action.RENAME) {
      message = {
        action: action,
        content: {
          username: this.user.name,
          previousUsername: params.previousUsername
        }
      };
    }

    this.socketService.send(message);
  }
}
