import {environment } from '../../environments/environment'

export const Config = {
        Service: {
            apiLocation: 'http://localhost/api/index.php',
            broadcastLocation: 'http://localhost:8080',
        },
        IsDev: (!environment.production)
}

