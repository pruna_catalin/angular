import { Routes } from '@angular/router';
import { AuthGuard } from '../_helpers';
import { UsersComponent} from '../users/user-list/users.component';

export const CoreRoutes: Routes = [
    {
        path: 'dashboard',
        // loadChildren: () => DashboardModule ,
        // loadChildren:  './dashboard/dashboard.module#DashboardModule',
        // '../dashboard/dashboard.module#DashboardModule'
        // '../dashboard/dashboard.module#DashboardModule'
        loadChildren: () => import('../dashboard/dashboard.module').then(mod => mod.DashboardModule),
        canActivate: [AuthGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'users',
        children: [
            {
                path: 'sandbox',
                loadChildren:  '../users/users.module#UsersModule'
            },
            {
                path: 'under',
                loadChildren:  '../users/users.module#UsersModule'
            } // url: about/item
        ],
        canActivate: [AuthGuard],
        canLoad: [AuthGuard]
    },

    { path: 'test', loadChildren:  '../dashboard/dashboard.module#DashboardModule#other' }
];
