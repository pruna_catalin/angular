import {ActivatedRoute, Params, QueryParamsHandling} from '@angular/router';

interface NavigationExtras {
    relativeTo?: ActivatedRoute | null
    queryParams?: Params | null
    fragment?: string
    preserveQueryParams?: boolean
    queryParamsHandling?: QueryParamsHandling | null
    preserveFragment?: boolean
    skipLocationChange?: boolean
    replaceUrl?: boolean
    state?: { [k: string]: any}
}
