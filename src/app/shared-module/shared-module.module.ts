import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthModule} from '../auth/auth.module';
import { HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [  ],
  imports: [
  ],
  exports: [
      CommonModule,
      AuthModule,
      HttpClientModule
  ],
  providers : [  ]
})
export class SharedModule {
}
