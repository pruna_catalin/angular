import {NgModule, Injectable} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import {AuthComponent} from './auth.component';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {DogWatcher} from '../_services/watch.services';
import {SocketService} from '../_services/socket.services';
import {AlertService, AuthenticationService} from '../_services';
import { ReactiveFormsModule } from '@angular/forms';
@Injectable()
@NgModule({
  declarations: [
    AuthComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    CommonModule,
    AuthComponent
  ],
  providers : []
})

export class AuthModule {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  constructor(private http: HttpClient, private watcher: DogWatcher, private alertService: AlertService,
              private router: Router, private socket: SocketService) {
    console.log(localStorage.getItem('currentUser') , localStorage.getItem('currentPermisions'));
  }

}

// export default { AuthModule, AuthComponent };
