import { HttpClient } from '@angular/common/http';
import 'rxjs-compat/add/operator/map';
export class Request {
    constructor (private http: HttpClient) {}
    sendCall(params: any): any {
        try {
            const method: string = params.method.toLowerCase();
            const location: string = config.Service.apiLocation + params.target;
            console.log(location);
            console.log('PARAMS:', params);
            const options = {
                headers: params.headers
            };

            // Call the webservice
            if (method === 'get') {
                return this.http.get(location, options).map(
                    (getResponse) => {
                        return getResponse;
                    },
                    (error) => {
                        console.log(error);
                        return error;
                    }
                );
            } else {
                return this.http.post(location, params.payload, options).map(
                    (postResponse) => {
                        return postResponse;
                    },
                    (error) => {
                        console.log(error);
                        return error;
                    }
                );
            }
        } catch (e) {
            console.log(e.message);
        }
    }
}
