import { NgModule  } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CoreRoutes } from '../_config/route';
import { TableListComponent } from '../table-list/table-list.component';
import { TypographyComponent } from '../typography/typography.component';
import { IconsComponent } from '../icons/icons.component';
import { MapsComponent } from '../maps/maps.component';
import { NotificationsComponent } from '../notifications/notifications.component';
import { UpgradeComponent } from '../upgrade/upgrade.component';

import {UsersComponent} from '../users/user-list/users.component';
import {UserDetailsComponent} from '../users/user-details/user-details.component';
import {ChatModule} from '../chat/chat.module';
import {SocketService} from '../_services/socket.services';
@NgModule({
    imports: [
        CommonModule,
        ChatModule,
        RouterModule.forChild(CoreRoutes)
    ],
    declarations: [
        TableListComponent,
        TypographyComponent,
        IconsComponent,
        MapsComponent,
        NotificationsComponent,
        UpgradeComponent,
        UsersComponent,
        UserDetailsComponent
    ],
    providers : [
        SocketService
    ]
})

export class CoreModule {}
